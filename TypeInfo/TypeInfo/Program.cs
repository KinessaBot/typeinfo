﻿using System;
using System.Reflection;

namespace TypeInfo
{
    class Program
    {
        public abstract class TypeInfo
        {
            static public void ShowFields(Type type)  //simply shows fileds
            {
                FieldInfo[] fields = type.GetFields();
                foreach (FieldInfo field in fields)
                {
                    Console.WriteLine(field.Name + " " + field.FieldType);
                }
            }

            static public void ShowInterfaces(Type type) //simply shows interfaces
            {
                Type[] interfaces = type.GetInterfaces();
                foreach (Type inter in interfaces)
                {
                    Console.WriteLine(inter.Name);
                }
            }

            static public void ShowMethods(Type type) //simply shows methods
            {
                MethodInfo[] methods = type.GetMethods();
                foreach (MethodInfo method in methods)
                {
                    if (method.IsConstructor)
                        Console.Write("Constructor ");
                    Console.Write(method.Name + " ");
                    ParameterInfo[] parametrs = method.GetParameters();
                    foreach (ParameterInfo parameter in parametrs)
                        Console.Write(parameter.Name + " " + parameter.ParameterType + " ");
                    Console.WriteLine("|| returns "+method.ReturnParameter.Name + " " + method.ReturnParameter.ParameterType);
                }
            }

            static public void ShowEnums(Type type)  //simply shows enums
            {
                    Type[] types = type.GetNestedTypes();
                    foreach (Type t in types)
                        if(t.IsEnum)
                            Console.WriteLine(t.Name);
            }
        }

        public class OnlyEnum
        {
            public enum WorldSides
            {
                East,
                West,
                North,
                South
            }
        }

        static void Main(string[] args)
        {
            Type typ1 = typeof(String);
            Type typ2 = typeof(OnlyEnum);


            //calling methods for string type
            Console.WriteLine("String");
            Console.WriteLine("Fileds:");
            TypeInfo.ShowFields(typ1);
            Console.WriteLine("Methods:");
            TypeInfo.ShowMethods(typ1);
            Console.WriteLine("Interfaces:");
            TypeInfo.ShowInterfaces(typ1);
            Console.WriteLine("Enums:");
            TypeInfo.ShowEnums(typ1);

            //calling methods for OnlyEnum type
            Console.WriteLine();
            Console.WriteLine("OnlyEnum");
            Console.WriteLine("Fileds:");
            TypeInfo.ShowFields(typ2);
            Console.WriteLine("Methods:");
            TypeInfo.ShowMethods(typ2);
            Console.WriteLine("Interfaces:");
            TypeInfo.ShowInterfaces(typ2);
            Console.WriteLine("Enums:");
            TypeInfo.ShowEnums(typ2);

            Console.ReadLine();
        }
    }
}
